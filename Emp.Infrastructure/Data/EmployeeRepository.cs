﻿using Emp.Core.Entity;
using Emp.Core.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emp.Infrastructure.Data
{
    public interface IEmployeeRepository:IRepository<Employee>
    {

    }
    public class EmployeeRepository : IEmployeeRepository
    {
        EmployeeDbContext _context = new EmployeeDbContext();
        public void Delete(int id)
        {
            Employee employee = _context.Employees.Find(id);
            _context.Employees.Remove(employee);
            _context.SaveChanges(); 
        }

        public IEnumerable<Employee> Get()
        {
            return _context.Employees.ToList();
        }

        public Employee GetById(int id)
        {
            return _context.Employees.Find(id);
        }

        public void Insert(Employee entity)
        {
            _context.Employees.Add(entity);
            _context.SaveChanges();
        }

        public void Update(Employee entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}
