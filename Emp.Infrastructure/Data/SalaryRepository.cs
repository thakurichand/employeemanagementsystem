﻿using Emp.Core.Entity;
using Emp.Core.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emp.Infrastructure.Data
{
    public interface ISalaryRepspository : IRepository<Salary>
    {
         void savechanges();

    }
    public class SalaryRepository : ISalaryRepspository
    {
        EmployeeDbContext _context = new EmployeeDbContext();

        public void Delete(int id)
        {
            Salary salary = _context.Salaries.Find(id);
            _context.Salaries.Remove(salary);
            _context.SaveChanges();
            
        }

        public IEnumerable<Salary> Get()
        {
            return _context.Salaries.ToList();
        }

        public Salary GetById(int id)
        {
            return _context.Salaries.Find(id);
        }

        public void Insert(Salary entity)
        {
            _context.Salaries.Add(entity);
            _context.SaveChanges();
        }

        public void Update(Salary entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();

        }
        public void savechanges()
        {
             _context.SaveChanges();
        }
    }
}
