﻿using Emp.Core.Entity;
using Emp.Core.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emp.Infrastructure.Data
{
    public interface IAddressRepository : IRepository<Address>
    {

    }
    public class AddressRepository : IAddressRepository
    {
        EmployeeDbContext _context = new EmployeeDbContext();
        public void Delete(int id)
        {
            Address address = _context.Addresses.Find(id);
            _context.Addresses.Remove(address);
            _context.SaveChanges();
        }

        public IEnumerable<Address> Get()
        {
            return _context.Addresses.ToList();
        }

        public Address GetById(int id)
        {
            return _context.Addresses.Find(id);
        }

        public void Insert(Address entity)
        {
            _context.Addresses.Add(entity);
            _context.SaveChanges();
        }

        public void Update(Address entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}
