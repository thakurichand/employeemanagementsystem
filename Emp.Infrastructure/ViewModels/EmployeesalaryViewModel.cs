﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Emp.Core.Entity.EmpoyeeSalaryViewModel
{
       public  class EmployeesalaryViewModel
    {
        public int SalaryId { get; set; }
        public int EmployeeId { get; set; }
        [Required(ErrorMessage = "Please enter Basic")]
        public string Basic { get; set; }
        [Required(ErrorMessage = "Please enter Allowance")]
        public string Allowance { get; set; }
        [Required(ErrorMessage = "Please enter CommunicationAllowance")]
        public string CommunicationAllowance { get; set; }
        public IEnumerable<SelectListItem> Employees { get; set; }
        public string EmployeeName { get; set; }

    }
}
