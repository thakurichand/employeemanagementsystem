﻿using EmployeeManagementSystemm.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeManagementSystemm.Services.EmployeeServices
{
    public interface IEmployeeService
    {
        IEnumerable<EmployeeViewModel> GetAll();
        EmployeeViewModel Details(int id);
    }
}