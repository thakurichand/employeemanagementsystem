﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Emp.Core.Entity;
using Emp.Infrastructure.Data;
using EmployeeManagementSystemm.ViewModels;

namespace EmployeeManagementSystemm.Services.EmployeeServices
{
    public class EmployeeService : IEmployeeService
    {
        private EmployeeRepository db = new EmployeeRepository();

        public EmployeeViewModel Details(int id)
        {
           
            Employee employee = db.GetById(Convert.ToInt32(id));
            EmployeeViewModel evm = new EmployeeViewModel()
            {
                DOB = employee.DOB,
                DOJoin = employee.DOJoin,
                EmployeeId = employee.EmployeeId,
                FatherName = employee.FatherName,
                Name = employee.Name
            };
            return evm;
        }

      

        public IEnumerable<EmployeeViewModel> GetAll()
        {
            var Employees = (from e in db.Get()
                             select new EmployeeViewModel
                             {
                                 DOB = e.DOB,
                                 DOJoin = e.DOJoin,
                                 EmployeeId = e.EmployeeId,
                                 FatherName = e.FatherName,
                                 Name = e.Name
                             }).ToList();
            return Employees;
        }
    }
}