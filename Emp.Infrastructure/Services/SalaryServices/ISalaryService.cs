﻿using Emp.Core.Entity.EmpoyeeSalaryViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emp.Infrastructure.Services.SalaryServices
{
  public  interface ISalaryService
    {
        EmployeesalaryViewModel CreateSalary();
        void PostSalary(EmployeesalaryViewModel esvm);
        IEnumerable<EmployeesalaryViewModel> GetAllSalary();
        EmployeesalaryViewModel GetEmployeeSalaryById(int id);
     
    }
}
