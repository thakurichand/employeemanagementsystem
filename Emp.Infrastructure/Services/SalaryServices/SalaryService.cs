﻿using Emp.Core.Entity;
using Emp.Core.Entity.EmpoyeeSalaryViewModel;
using Emp.Infrastructure.Data;
using Emp.Infrastructure.Services.SalaryServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmployeeManagementSystemm.Services.SalaryServices
{
    public class SalaryService : ISalaryService
    {
        IEmployeeRepository erepo = new EmployeeRepository();
        ISalaryRepspository salaryrepo = new SalaryRepository();

        public EmployeesalaryViewModel CreateSalary()
        {
            EmployeesalaryViewModel esvm = new EmployeesalaryViewModel();
            esvm.Employees = (from e in erepo.Get()
                              select new SelectListItem
                              {
                                  Text = e.Name,
                                  Value = e.EmployeeId.ToString()

                              }).ToList();
            return esvm;
        }

        public EmployeesalaryViewModel CreateSalaryGet()
        {
            throw new NotImplementedException();
        }

      
        public IEnumerable<EmployeesalaryViewModel> GetAllSalary()
        {
            var Employeesalary = (from e in erepo.Get()
                                  join s in salaryrepo.Get()
                                  on e.EmployeeId equals s.EmployeeId
                                  select new EmployeesalaryViewModel
                                  {
                                      SalaryId = s.SalaryId,
                                      EmployeeName = e.Name,
                                      Allowance = s.Allowance,
                                      Basic = s.Basic,
                                      CommunicationAllowance = s.CommunicationAllowance
                                  }).ToList();
            return Employeesalary;
        }

        public EmployeesalaryViewModel GetEmployeeSalaryById(int id)
        {
            Salary sal = new Salary();
            sal = salaryrepo.GetById(Convert.ToInt32(id));
            EmployeesalaryViewModel esvm = new EmployeesalaryViewModel()
            {
                Allowance = sal.Allowance,
                SalaryId = sal.SalaryId,
                Basic = sal.Basic,
                CommunicationAllowance = sal.CommunicationAllowance,
                EmployeeId = sal.EmployeeId
            };
            var Employeename = (from e in erepo.Get()
                                join s in salaryrepo.Get()
                                on e.EmployeeId equals s.EmployeeId
                                where e.EmployeeId == sal.EmployeeId
                                select e.Name).FirstOrDefault();
            esvm.EmployeeName = Employeename;
            return esvm;
        }

        public void PostSalary(EmployeesalaryViewModel esvm)
        {
            Salary salary = new Salary()
            {
                Allowance = esvm.Allowance,
                Basic = esvm.Basic,
                EmployeeId = esvm.EmployeeId,
                CommunicationAllowance = esvm.CommunicationAllowance
            };
            salaryrepo.Insert(salary);
            salaryrepo.savechanges();
        }
    }
}