﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emp.Core.Interface
{
    public interface IRepository<T> where T:class
    {
        void Insert(T entity); 

        IEnumerable<T> Get();

        T GetById(int id);

        void Update(T entity); 

        void Delete(int id); 
    }
}
