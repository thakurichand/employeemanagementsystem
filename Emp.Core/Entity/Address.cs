﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emp.Core.Entity
{
   public class Address
    {
        [Key]
        public int AddressId { get; set; }
        public int EmployeeId { get; set; }
        [Required(ErrorMessage = "Please Enter PermanentAddress")]
        public string PermanentAddress { get; set; }
        [Required(ErrorMessage = "Please Enter CurrentAddress")]
        public string CurrentAddress { get; set; }
        [Required(ErrorMessage = "Please Enter Country")]
        public string Country { get; set; }
        [Required(ErrorMessage = "Please Enter State")]
        public string State { get; set; }
        [Required(ErrorMessage = "Please Enter City")]
        public string City { get; set; }
        [Required(ErrorMessage = "Please Enter Street")]
        public string Street { get; set; }
    }
}
