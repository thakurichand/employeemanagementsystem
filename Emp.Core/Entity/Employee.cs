﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emp.Core.Entity
{
   public class Employee
    {
        [Key]
        public int EmployeeId { get; set; }
        [Required(ErrorMessage = "Please enter FullName")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter FatherName")]
        public string FatherName { get; set; }
        [DisplayName("DateOfBirth")]
        [Required(ErrorMessage = "Please enter DateOfBirth")]
        public DateTime DOB { get; set; }
        [DisplayName("DateOfJoin")]
        public DateTime DOJoin { get; set; } = System.DateTime.Now;
        
    }
}
