﻿using Emp.Core.Entity;
using Emp.Core.Entity.EmpoyeeSalaryViewModel;
using Emp.Infrastructure.Data;
using Emp.Infrastructure.Services.SalaryServices;
using EmployeeManagementSystemm.Services.SalaryServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmployeeManagementSystemm.Controllers
{
    public class SalaryController : Controller
    {
        IEmployeeRepository erepo = new EmployeeRepository();
        ISalaryRepspository salaryrepo = new SalaryRepository();
        ISalaryService salaryService = new SalaryService();

        // GET: Salary
        public ActionResult Index()
        {
            var Employeesalary = salaryService.GetAllSalary();
            return View(Employeesalary);
        }

        // GET: Salary/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Salary/Create
        public ActionResult Create()
        {
            var EmpSalary = salaryService.CreateSalary();
            return View(EmpSalary);
        }

        // POST: Salary/Create
        [HttpPost]
        public ActionResult Create(EmployeesalaryViewModel esvm)
        {
            if (ModelState.IsValid)
            {
                salaryService.PostSalary(esvm);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }

        // GET: Salary/Edit/5
        public ActionResult Edit(int id)
        {
            var  esvm = salaryService.GetEmployeeSalaryById(Convert.ToInt32(id));
            return View(esvm);
        }

        // POST: Salary/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EmployeesalaryViewModel esvm)
        {
            if (ModelState.IsValid)
            {
                Salary salary = new Salary()
                {
                    SalaryId=esvm.SalaryId,
                    Allowance = esvm.Allowance,
                    Basic = esvm.Basic,
                    EmployeeId = esvm.EmployeeId,
                    CommunicationAllowance = esvm.CommunicationAllowance
                };
                salaryrepo.Update(salary);
            }
            return RedirectToAction("Index");
        }

        // GET: Salary/Delete/5
        public ActionResult Delete(int id)
        {
            var esvm = salaryService.GetEmployeeSalaryById(Convert.ToInt32(id));
            return View(esvm);
        }

        // POST: Salary/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, EmployeesalaryViewModel esvm)
        {
            Console.WriteLine(esvm.SalaryId);
            if (ModelState.IsValid)
            {
                Console.WriteLine(esvm.SalaryId);
                salaryrepo.Delete(Convert.ToInt32(esvm.SalaryId));
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }
    }
}
